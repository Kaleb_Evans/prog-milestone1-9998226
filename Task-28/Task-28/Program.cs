﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_28
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Please enter three words pressing ENTER after each. ");
            Console.WriteLine();
            Console.WriteLine();

            var a = Console.ReadLine();
            var b = Console.ReadLine();
            var c = Console.ReadLine();
            Console.WriteLine();

            var arr = new string[3] { a, b, c };

            Console.WriteLine();
            Console.WriteLine(a);
            Console.WriteLine();
            Console.WriteLine(b);
            Console.WriteLine();
            Console.WriteLine(c);
            Console.WriteLine();
        }

    }
}
