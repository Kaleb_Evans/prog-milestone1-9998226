﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_37
{
    class Program
    {
        static void Main(string[] args)
        {
            // How many hours you should study per week for a 15 credit paper
            // 10 hours of study per credit
            // 5 contract hours (lectures/classes) per week
            // Semester is 12 weeks long

            int credit = 1;

            Console.WriteLine();
            Console.WriteLine("How many hours per week should you study for a 15 credit paper? ");
            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine("1 credit = 10 hours per week");
            Console.WriteLine();

            Console.WriteLine("5 Contract hours per week (lectures/classes)");
            Console.WriteLine();

            Console.WriteLine("A semester is 12 weeks long");
            Console.WriteLine();

            Console.WriteLine("15 hours total (contract hours + expected study) per week");
            Console.WriteLine();

            Console.WriteLine($"15 hours x 12 weeks = {(credit*15) * (credit*12)} ");
            Console.WriteLine();
            Console.WriteLine($"{(credit*15) * (credit*12)} hours of study are expected per semester for a 15 credit paper");
            Console.WriteLine();



        }
    }
}
