﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_03
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, please enter 1 to convert KM to Miles or enter 2 to convert Miles into KM");

            int type = Convert.ToInt32(Console.ReadLine());

            if (type == 1)
            {
                    Console.Clear();
                    Console.WriteLine("KM to Mile converter.");
                    Console.WriteLine();
                    Console.WriteLine("Please enter a number.");

                    int km = Convert.ToInt32(Console.ReadLine());
                    const double mile = 0.6213;

                    Console.WriteLine($"{km} kms is {km * mile} miles");
            }


            else if (type == 2)
            {
                Console.Clear();
                Console.WriteLine("Mile to KM converter.");
                Console.WriteLine();
                Console.WriteLine("Please enter a number.");

                int mile2 = Convert.ToInt32(Console.ReadLine());
                const double km2 = 1.609344;

                Console.WriteLine($"{mile2} miles is {mile2 * km2} kms");
            }

            Console.ReadLine();

        }
    }
}

