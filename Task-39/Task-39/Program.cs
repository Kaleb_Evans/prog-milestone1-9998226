﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_39
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine();
            Console.WriteLine("How many Wednesday are there in a month?");
            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine("There are 4 weeks in a month");
            Console.WriteLine();

            Console.WriteLine("There is 1 Wednesday per week");
            Console.WriteLine();

            Console.WriteLine($"4 / 1 = {4 / 1} Wednesday's per week ");
            Console.WriteLine();

        }
    }
}
