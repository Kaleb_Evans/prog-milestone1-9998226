﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_02
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Hello, what date were you born?");

            var date = Console.ReadLine();
            Console.WriteLine($"You were born on {date}");
            
        }
    }
}
