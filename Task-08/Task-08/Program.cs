﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_08
{
    class Program
    {
        static void Main(string[] args)
        {
            var counter = 99; // Line one            

            for (int i = counter; i >= 0; i--) // Line two
            {
                if (i >= 0) // Line three
                {
                    var a = i; // Line four
                    Console.WriteLine($"{a}"); // Line five                                     
                }            
            }
        }
    }
}
