﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Task_21
{
    class Program
    {
        static void Main(string[] args)
        {
           var dict = new Dictionary<string, string>();

            dict.Add("March", "14th");
            dict.Add("June", "6th");
            dict.Add("May", "11th");
            dict.Add("November", "4th");
            

            var output = "";
            bool value = dict.TryGetValue("14th", out output);

            if (dict.ContainsKey("March"))
            {
                Console.WriteLine($"March's date is the {dict["March"]}");
            }


        }
    }
}
